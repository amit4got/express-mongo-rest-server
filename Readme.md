# **Express and MongoDB Server**
---

A simple express library which connects to MongoDB using Mongoose as it's driver.
This library will auto-generate the endpoints for: `Create`, `Retrieve One`, `Retrieve all`, `Update` & `Delete`.

Just define your mongoose model schemas and pass the path to the schemas to the function.
Also create new or override existing controller methods and endpoints.


```javascript
import express from 'express';

import EMRS from 'express-mongo-rest-server';

let server = express();

const schemasPath = './schemas';
const controllersPath = './controllers';
const routesPath = './routes';

server.models = {};
server.controllers = {};

// Load Models
server.models = EMRS.Models(schemasPath);

//Load the controllers
server.controllers = EMRS.Controllers(server.models, controllersPath);

//Load the routes
EMRS.Routes(server, Config('/api/baseUri'), routesPath);
```

## **Installation**
---

`npm i express-mongo-rest-server --save`


## **Quick Start**
---

All you need to do is define your collection schemas as _Mongoose Schemas_, and you will automatically get your REST CRUD operations set.

**Define your Schemas:**
> ./schemas/tenerife.js

```javascript
import { Schema } from 'mongoose';
import slug from 'slug';

const TenerifeSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        unique: true
    },
    slug: {
        type: String,
        unique: true
    },
    description: String
});

TenerifeSchema.methods = {};

TenerifeSchema.pre('save', function(next) {
    this.slug = slug(this.name);
    next();
});

module.exports = TenerifeSchema;
```

You can also override the Controller methods if you need any processing in the data before returning it back.

**Override or extend your controller methods:**
> ./controllers/tenerife.js

```javascript
import Debug from 'debug';
const debug = Debug('EMCR:FuerteventuraController');

// Define your controller methods here
// Override methods here
const TenerifeController = function(model) {
    let controller = {};

    controller.getExample = function(callback) {

        debug('Exampling');
        callback();
    };

    controller.get = function(callback) {

        debug('Calling get function override');
        model.findOne(function (error, data) {

            callback(error, data);
        });
    };

    return controller;
}

module.exports = TenerifeController;
```

Also, if you want to override or create new routes, you can do it easily.

**Override or extend your routes:**
> ./routes/tenerife.js

```javascript
import Debug from 'debug';
const debug = Debug('EMCR:TenerifeRoutes');

// Override all your controller methods here
module.exports = function(server, basePath, controllerName) {

    server.get(basePath, function(request, response) {

        server.controllers[controllerName].get(function(error, data) {

            if (error) return response.status(400).send(error);

            response.send(data);
        });
    });


    server.get(basePath + '/enjoy-tenerife', function(request, response) {

        response.send('Lets enjoy Tenerife');
    });

}
```

## **REST API**
---

Five routes will be automatically added to each one of your models:

**GET /api/v1/model**

**GET /api/v1/model/:id**

**POST /api/v1/model**

**PUT /api/v1/model/:id**

**DELETE /api/v1/model/:id**


#### **Note**
---

> This library uses Express as its Framework and Mongoose as the Object Data Mapping (ODM)
> This library is not perfect, at all, it's just a quick way to build simple software.
> There is no validation or security built-in in.