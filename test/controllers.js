import Chai from 'chai';
import Dir from 'node-dir';

import Config from '../config';
import { Models } from '../app/models/';
import { Controllers } from '../app/controllers/';

const Expect = Chai.expect;


// CONTROLLERS LIST
describe('Load Controllers', function(){

    it('should return the number of controllers loaded', function(done){

        const controllers = Controllers( { models: Models() } );
        const numberOfControllers = Object.keys(controllers).length;

        Expect(numberOfControllers).to.be.an('Number');
        done();
    });


    it('should return the number of OWN controllers or be undefined', function(done){

        Dir.files(__dirname + '/../app/controllers/controller', function (err, files) {

            if (files) {

                Expect(files.length).to.be.an('Number');
            } else {

                Expect(files).to.be.an('undefined');
            }

            done();
        });
    });
});

