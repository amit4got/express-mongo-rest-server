import Chai from 'chai';
import Dir from 'node-dir';

import Config from '../config';
import { Models } from '../app/models/';

const Expect = Chai.expect;


// MODELS LIST
describe('Load Models', function(){

    let numberOfModels;

    it('should return the number of models loaded', function(done){

        const models = Models();
        numberOfModels = Object.keys(models).length;

        Expect(numberOfModels).to.be.an('Number');
        done();
    });

    it('should match with the number of files', function(done){


        Dir.files(__dirname + '/../app/models/schemas', function (err, files) {

            Expect(numberOfModels).to.be.equal(files.length);
            done();
        });
    });
});

