var Fs = require('fs');
var Debug = require('debug')('EMRS:Routes');
var Utils = require('../lib/utils');

// Setup CRUD routes per model
module.exports = function Routes(server, baseURI, routesPath) {

    var baseControllerPath = Utils.traceCaller() + '/' + routesPath.replace(/^\//, '').replace(/\/$/, '') + '/';

    if (! baseURI) baseURI = '/';
    else baseURI = '/' + baseURI.replace(/^\//, '').replace(/\/$/, '') + '/';


    Object.keys(server.controllers).forEach(function(controllerName) {

        var controller = server.controllers[controllerName];
        var basePath = baseURI + controllerName;

        /** OVERRIDE ANY CRUD OR OTHER EXISTING METHODS **/
        var controllerPath = baseControllerPath + controllerName.toLowerCase() + '.js';

        try {

            if (Fs.statSync(controllerPath)) {

                Debug('Loading routes: ', controllerName);
                require(controllerPath)(server, basePath, controllerName.toLowerCase());
            }

        } catch (error) { // We don't want to worry about files not existing

            Debug(`Handled error: Routes file for "${controllerName}" controller does not exist. Routes will not be overridden for path ${controllerPath}`);
        }


        /** LOAD ROUTES PROPER METHODS **/
        // Create new
        server.post(basePath, function(request, response) {

            const data = request.body;

            controller.create(data, function(error, data) {

                if (error) return response.status(400).send({error: error});

                response.send(data);
            });
        });


        // Get all
        server.get(basePath, function(request, response) {

            controller.get(function(error, data) {

                if (error) return response.status(400).send({error: error});

                response.send(data);
            });
        });


        // Update existing
        server.put(basePath + '/:id', function(request, response) {

            const data = request.body;
            const id = request.params.id;

            controller.update(id, data, function(error, data) {

                if (error) return response.status(400).send({error: error});

                response.send(data);
            });
        });


        // Delete
        server.delete(basePath + '/:id', function(request, response) {
            const id = request.params.id;

            controller.delete(id, function(error, data) {

                if (error) return response.status(400).send({error: error});

                response.status(410).send(data);
            });
        });


        // Get by Id
        server.get(basePath + '/:id', function(request, response) {

            const id = request.params.id;

            controller.getById(id, function(error, data) {

                if (error) return response.status(400).send({error: error});

                response.send(data);
            });
        });
    });

}

