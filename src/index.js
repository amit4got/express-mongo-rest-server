module.exports = {

    Models: require('./models'),
    Controllers: require('./controllers'),
    Routes: require('./routes')
};
