var Mongoose = require('mongoose');
var Path = require('path');
var Callsite = require('callsite');

// Return if the data passed is a valid Mongoose Object ID
module.exports = {
    isMongooseObjectId: function (data) {

        return Mongoose.Types.ObjectId.isValid(data);
    },

    traceCaller: function (trace) {

        trace = trace || 2;

        var stack = Callsite();
        var requester = stack[trace].getFileName();

        return Path.dirname(requester);
    }
};



