var Fs = require('fs');
var Utils = require('../lib/utils');
var Debug = require('debug')('EMRS:Controllers');


// Create a CRUD controller per model
module.exports = function Controllers(models, controllersPath) {

    var controllers = {};

    var baseControllerPath = Utils.traceCaller() + '/' + controllersPath.replace(/^\//, '').replace(/\/$/, '') + '/';


    Object.keys(models).forEach(function(modelName) {


        var model = models[modelName];


        controllers[modelName] = {};

        // Create
        controllers[modelName].create = function(data, callback) {

            Debug('Calling create: ', modelName);

            // Create virtual document
            var _model = new model(data);

            // Insert new Category
            _model.save(data, function(error, _data) {

                if (error) {

                    Debug('Creating error');
                    var errorMessage = error;

                    if (error.errmsg) errorMessage = error.errmsg;
                    if (error.errors) errorMessage = error.errors;

                    return callback(errorMessage);
                }

                callback(null, _data);
            });
        }


        // Retrieve / Get
        controllers[modelName].get = function(callback) {

            Debug('Calling get: ', modelName);
            model.find(function (error, data) {

                callback(error, data);
            });
        }


        // Update
        controllers[modelName].update = function(id, data, callback) {

            Debug('Calling update: ', modelName);

            // Check if ID passed is a correct Mongoose Object ID
            if (! Utils.isMongooseObjectId(id)) return callback('Id should be a valid Mongoose Object ID: ' + id);

            // @TODO: Slug must be updated when name changes
            model.findByIdAndUpdate(id, data, function(error, _data) {

                if (error) return callback(error);

                if (! _data) return callback('Does not exist');

                callback(null, 'Done');
            });
        }


        // Delete
        controllers[modelName].delete = function(id, callback) {

            Debug('Calling delete: ', modelName);

            // Check if ID passed is a correct Mongoose Object ID
            if (! Utils.isMongooseObjectId(id)) return callback('Id should be a valid Mongoose Object ID: ' + id);

            model.findByIdAndRemove(id, function(error, _data) {

                if (error) return callback(error);

                if (! _data) return callback('Does not exist');

                callback(null, 'Done');
            });
        };


        // Get one by Id
        controllers[modelName].getById = function(id, callback) {

            Debug('Calling get one by id: ', modelName);

            // Check if ID passed is a correct Mongoose Object ID
            if (! Utils.isMongooseObjectId(id)) return callback('Id should be a valid Mongoose Object ID: ' + id);

            model.findById(id, function(error, _data) {

                if (error) return callback(error);

                if (! _data) return callback('Does not exist');

                callback(error, _data);
            });
        };

        // /** LOAD CONTROLLER PROPER METHODS **/
        /** OVERRIDE ANY CRUD OR OTHER EXISTING METHODS **/
        var controllerMethods;

        var nextControllerPath = baseControllerPath + modelName.toLowerCase() + '.js';

        try {

            if (Fs.statSync(nextControllerPath)) {

                controllerMethods = require(nextControllerPath)(model);

                Debug('Loading controller: ' + modelName.toLowerCase());
                Object.keys(controllerMethods).forEach(function(method) {

                    controllers[modelName][method] = controllerMethods[method];
                });

            }
        } catch (error) { // We don't want to worry about files not existing

            Debug(`Handled error: Controller file for "${modelName}" model does not exist. Controller actions will not be overridden for path ${nextControllerPath}`);
        }

    });

    return controllers;
}
