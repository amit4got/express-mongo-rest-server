var pluralize = require('pluralize');
var mongoose = require('mongoose');
var Fs = require('fs');
var Debug = require('debug')('EMRS:Models');
var Utils = require('../lib/utils');

module.exports = function Models(schemasPath) {

    schemasPath = Utils.traceCaller() + '/' + schemasPath.replace(/^\//, '').replace(/\/$/, '') + '/';

    var models = {};

    // Load all models from the schemas folders
    var fileNames = Fs.readdirSync(schemasPath);


    // Loop through the filenames, trim .js extension and load model
    fileNames.forEach(function(model) {

        var modelName = model.slice(0, -3);

        models[modelName] = {};
        Debug('Loading model: ' + modelName.toLowerCase())

        var schema = require(schemasPath + modelName.toLowerCase());

        models[modelName] = mongoose.model(pluralize(modelName.toLowerCase()), schema);
    });

    return models;
};